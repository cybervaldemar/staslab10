#include "cmath.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

int rkf_fun(int n, double t, double x[], double dx[])//Функция дли вызова методов Рунге-Кутты
{
    dx[0] = -430 * x[0] - 12000 * x[1] + exp(-10*t);
    dx[1] = x[0] + log(1 + 100 * (t * t));
    return 0;
}

void rk_2(int (*F)(int n, double t, double y[], double yp[]), int n, double x[],
    double *t, double t_out, double h_int)// Метод Рунге-Кутты 2-й степени точности
{
    double temp1[n];//f(Tn,Zn)
    double temp2[n];//f(Tn + 2/3, Zn + 2/3)
    double preresult[n];//Zn + 2/3

    //while (*t + h_int < t_out + h_int * 0.0001)//Получение значений до тех пор,
        //пока не кончится промежуток интегрированния
   // {
        F(n, *t, x, temp1);//получение temp1

        for (int i = 0; i < n; ++i)
        {
            preresult[i] = x[i] + 2/3 * h_int * temp1[i];
        }

        F(n, *t + h_int * 2/3, preresult, temp2);//получение K2/h_int

        for (int i = 0; i < n; ++i)//получение значени вектора x в новой точке
        {
            x[i] += (temp1[i] + 3 * temp2[i]) * h_int / 4;
        }

        *t += h_int;
  //  }
}

extern "C" void calculateDiff() {
    std::ofstream out_file("out.txt");//Открытие файла для вывода результатов

    if (!out_file)//Проверка на ошибку открытия
    {
        std::cout << "Could not open(create) file.\n";
        return;
    }

    int fail;//Флаг ошибки в RKF
    int NEQN = 2;//Количество уравнений в системе
    rkfinit(NEQN, &fail);//Выделение памяти для работы метода

    switch(fail)//Проверка выделения памяти
    {
        case 0:
            out_file << "Successful allocation of RKF45 workspace.\n";
        break;

        case 1:
            out_file << "Could not allocate memory for RKF45 workspace.\n";
            return;
        break;

        case 2:
            out_file << "Illegal value for NEQN \n";
            return;
        break;

        default:
            out_file << "Something went wrong\n";
            return;
    }

    //установка параметров для вызова RKF45
    NEQN = 2;//Количество уравнений
    double x[] = {3, -1};//Начальные значениия
    double dx[2];//Вектор необходимый для работы RKF45
    double t_start = 0;//Начальное значение T
    double t_finish = 0.15;//Конечное значение
    double h_print = 0.0075;//Шаг печати
    double relerr = 1e-4;//Относительная погрешность
    double abserr = 1e-4;//Абсолютная погрешность
    double h;//параметр необходимый для работы RKF45
    int NFE;//Количество вызовов функции
    int max_NFE = 5000;//максимальное количество вызовов функции
    int iflag = 1;//флаг 1 - обычное применение метода

    out_file << "-------------------------------------------------------\n";
    out_file << "RKF45\n";
    out_file << "-------------------------------------------------------\n";
    out_file << std::fixed;

    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rkf45(rkf_fun, NEQN, x, dx, &t_start, t_start + h_print,
            &relerr, abserr, &h, &NFE, max_NFE, &iflag);//Вызов RKF45

        if (iflag != 2)//Проверка ошибки в работе RKF45
        {
            out_file << "Error in rkf45? iflag = " << iflag << '\n';
            return;
        }

        out_file << "T = " << std::setprecision(5) << t_start; //Вывод результатов
        out_file << "; X[1] = " << std::setprecision(12) << x[0];
        out_file << "; X[2] = " << std::setprecision(12) << x[1];
        out_file << "; iflag = " << iflag << '\n';
    }

    rkfend();//Освобождение выделенной для RKF45 памяти

    //Метод Рунге-Кутты 2 степени точности
    out_file << "-------------------------------------------------------\n";
    out_file << "RK 2; h_int = " << h_print << "\n";
    out_file << "-------------------------------------------------------\n";

    NEQN = 2;//Количество уравнений
    x[0] = 3;//Начальные значения
    x[1] = -1;
    t_start = 0;//Начальное значение T
    t_finish = 0.15;//Конечное значение
    h_print = 0.0075;//Шаг печати
    double h_int = 0.0075;//Шаг интегрирования

    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rk_2(rkf_fun, NEQN, x, &t_start, t_start + h_print, h_int);//Вызов метода
        
        out_file << "T = " << std::setprecision(5) << t_start;//Вывот результатов
        out_file << "; X[1] = " << std::setprecision(12) << x[0];
        out_file << "; X[2] = " << std::setprecision(12) << x[1] << '\n';
    }

    NEQN = 2;//Количество уравнений
    x[0] = 3;//Начальные значения
    x[1] = -1;
    t_start = 0;//Начальное значение T
    t_finish = 0.15;//Конечное значение
    h_print = 0.00075;//Шаг печати
    h_int = 0.00075;//Шаг интегрирования

    //Метод Рунге-Кутты 2 степени точности с другим(меньшим) шагом интегрирования
    out_file << "-------------------------------------------------------\n";
    out_file << "RK 2; h_int = " << h_int << "\n";
    out_file << "-------------------------------------------------------\n";

    int step = 0;
    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rk_2(rkf_fun, NEQN, x, &t_start, t_start + h_print, h_int);//Вызов метода


        step++;


        if (step == 10) {
            out_file << "T = " << std::setprecision(5) << t_start;//Вывот результатов
            out_file << "; X[1] = " << std::setprecision(12) << x[0];
            out_file << "; X[2] = " << std::setprecision(12) << x[1] << '\n';
            step = 0;
        }
    }
}

/*int main(int , char const * [])
{
    std::ofstream out_file("out.txt");//Открытие файла для вывода результатов

    if (!out_file)//Проверка на ошибку открытия
    {
        std::cout << "Could not open(create) file.\n";
        return 1;
    }

    int fail;//Флаг ошибки в RKF
    int NEQN = 2;//Количество уравнений в системе
    rkfinit(NEQN, &fail);//Выделение памяти для работы метода

    switch(fail)//Проверка выделения памяти
    {
        case 0:
            out_file << "Successful allocation of RKF45 workspace.\n";
        break;

        case 1:
            out_file << "Could not allocate memory for RKF45 workspace.\n";
            return 2;
        break;

        case 2:
            out_file << "Illegal value for NEQN \n";
            return 3;
        break;

        default:
            out_file << "Something went wrong\n";
            return 4;
    }

    //установка параметров для вызова RKF45
    NEQN = 2;//Количество уравнений
    double x[] = {3, -1};//Начальные значениия
    double dx[2];//Вектор необходимый для работы RKF45
    double t_start = 0;//Начальное значение T
    double t_finish = 0.15;//Конечное значение
    double h_print = 0.0075;//Шаг печати
    double relerr = 1e-4;//Относительная погрешность
    double abserr = 1e-4;//Абсолютная погрешность
    double h;//параметр необходимый для работы RKF45
    int NFE;//Количество вызовов функции
    int max_NFE = 5000;//максимальное количество вызовов функции
    int iflag = 1;//флаг 1 - обычное применение метода

    out_file << "-------------------------------------------------------\n";
    out_file << "RKF45\n";
    out_file << "-------------------------------------------------------\n";
    out_file << std::fixed;

    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rkf45(rkf_fun, NEQN, x, dx, &t_start, t_start + h_print,
            &relerr, abserr, &h, &NFE, max_NFE, &iflag);//Вызов RKF45

        if (iflag != 2)//Проверка ошибки в работе RKF45
        {
            out_file << "Error in rkf45? iflag = " << iflag << '\n';
            return 5;
        }

        out_file << "T = " << std::setprecision(5) << t_start; //Вывод результатов
        out_file << "; X[1] = " << std::setprecision(12) << x[0];
        out_file << "; X[2] = " << std::setprecision(12) << x[1];
        out_file << "; iflag = " << iflag << '\n';
    }

    rkfend();//Освобождение выделенной для RKF45 памяти

    //Метод Рунге-Кутты 2 степени точности
    out_file << "-------------------------------------------------------\n";
    out_file << "RK 2; h_int = " << h_print << "\n";
    out_file << "-------------------------------------------------------\n";

    NEQN = 2;//Количество уравнений
    x[0] = 3;//Начальные значения
    x[1] = -1;
    t_start = 0;//Начальное значение T
    t_finish = 0.15;//Конечное значение
    h_print = 0.0075;//Шаг печати
    double h_int = 0.0075;//Шаг интегрирования

    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rk_2(rkf_fun, NEQN, x, &t_start, t_start + h_print, h_int);//Вызов метода
        
        out_file << "T = " << std::setprecision(5) << t_start;//Вывот результатов
        out_file << "; X[1] = " << std::setprecision(12) << x[0];
        out_file << "; X[2] = " << std::setprecision(12) << x[1] << '\n';
    }

    NEQN = 2;//Количество уравнений
    x[0] = 3;//Начальные значения
    x[1] = -1;
    t_start = 0;//Начальное значение T
    t_finish = 0.15;//Конечное значение
    h_print = 0.00075;//Шаг печати
    h_int = 0.00075;//Шаг интегрирования

    //Метод Рунге-Кутты 2 степени точности с другим(меньшим) шагом интегрирования
    out_file << "-------------------------------------------------------\n";
    out_file << "RK 2; h_int = " << h_int << "\n";
    out_file << "-------------------------------------------------------\n";

    int step = 0;
    while(t_start + h_print < t_finish + h_print * 0.0001)
    {
        rk_2(rkf_fun, NEQN, x, &t_start, t_start + h_print, h_int);//Вызов метода


        step++;


        if (step == 10) {
            out_file << "T = " << std::setprecision(5) << t_start;//Вывот результатов
            out_file << "; X[1] = " << std::setprecision(12) << x[0];
            out_file << "; X[2] = " << std::setprecision(12) << x[1] << '\n';
            step = 0;
        }
    }

    return 0;
}*/
